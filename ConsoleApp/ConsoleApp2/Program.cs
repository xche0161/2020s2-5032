﻿using System;
using System.Net;
using System.Net.Mail;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            sendEmail();
            Console.ReadKey();
        }
        public async static void sendEmail()
        {
            var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress("chenxiaoqian177@gmail.com"));
            // replace with valid value 
            message.From = new MailAddress("xche0161@student.monash.edu");
            // replace with valid value
            message.Subject = "Your email subject";
            message.Body = string.Format(body, "chen", "xche0161@student.monash.edu",
            "333333333333333");
            //message.IsBodyHtml = true;
            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential { };
                smtp.Credentials = credential;
                smtp.Host = "smtp.monash.edu.au";
                await smtp.SendMailAsync(message);
            }
        }
    }
}
